FROM python:3.10

COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip install -r requirements.txt

COPY . /app

ENV PORT 5000

EXPOSE 5000

ENV FLASK_APP=app.py

CMD ["python", "app.py"]
